package ru.tsc.vinokurov.tm.model;

import java.util.UUID;

public class AbstractModel {

    private String id = UUID.randomUUID().toString();

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

}
