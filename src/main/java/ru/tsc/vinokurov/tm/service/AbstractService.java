package ru.tsc.vinokurov.tm.service;

import ru.tsc.vinokurov.tm.api.repository.IRepository;
import ru.tsc.vinokurov.tm.api.service.IService;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.exception.entity.ItemNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.IdEmptyException;
import ru.tsc.vinokurov.tm.exception.field.IndexIncorrectException;
import ru.tsc.vinokurov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return repository.findAll(sort.getComparator());
    }

    @Override
    public M add(final M item) {
        if (item == null) throw new ItemNotFoundException();
        return repository.add(item);
    }

    @Override
    public M remove(final M item) {
        if (item == null) throw new ItemNotFoundException();
        return repository.remove(item);
    }

    @Override
    public M removeById(final String id) {
        final M item = findOneById(id);
        if (item == null) throw new ItemNotFoundException();
        return repository.remove(item);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M item = findOneByIndex(index);
        if (item == null) throw new ItemNotFoundException();
        return repository.remove(item);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public M findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index >= repository.size()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @Override
    public M findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public int size() {
        return repository.size();
    }

}
