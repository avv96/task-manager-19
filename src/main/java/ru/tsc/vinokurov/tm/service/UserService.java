package ru.tsc.vinokurov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.api.service.IUserService;
import ru.tsc.vinokurov.tm.enumerated.Role;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByEmailException;
import ru.tsc.vinokurov.tm.exception.entity.UserExistsByLoginException;
import ru.tsc.vinokurov.tm.exception.field.EmailEmptyException;
import ru.tsc.vinokurov.tm.exception.field.PasswordEmptyException;
import ru.tsc.vinokurov.tm.exception.field.RoleEmptyException;
import ru.tsc.vinokurov.tm.exception.field.UserIdEmptyException;
import ru.tsc.vinokurov.tm.model.User;
import ru.tsc.vinokurov.tm.util.HashUtil;

import java.util.List;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {


    public UserService(final IUserRepository userRepository) {
        super(userRepository);
    }

    @Override
    public User findOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new UserIdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    public User findOneByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        return repository.findOneByLogin(login);
    }

    @Override
    public User findOneByEmail(final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        return repository.findOneByEmail(email);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return repository.remove(user);
    }


    @Override
    public User removeByLogin(final String login) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        final User user = findOneByLogin(login);
        if (user == null) return null;
        return repository.remove(user);
    }

    @Override
    public User create(final String login, final String password) {
        if (StringUtils.isEmpty(login)) throw new EmailEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        if (repository.existsByLogin(login)) throw new UserExistsByLoginException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt((password)));
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (StringUtils.isEmpty(email)) throw new EmailEmptyException();
        if (repository.existsByEmail(email)) throw new UserExistsByEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email, final Role role) {
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password, email);
        if (user == null) return null;
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        if (StringUtils.isEmpty(password)) throw new PasswordEmptyException();
        final User user = findOneById(userId);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(
            final String userId, final String firstName,
            final String lastName, final String middleName
    ) {
        if (StringUtils.isEmpty(userId)) throw new UserIdEmptyException();
        final User user = findOneById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
