package ru.tsc.vinokurov.tm.command.binding;

import ru.tsc.vinokurov.tm.util.TerminalUtil;

public final class UnbindTaskFromProject extends AbstractBindingCommand {

    public static final String NAME = "task-unbind-from-project";

    public static final String DESCRIPTION = "Unbind task from project.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId, taskId);
    }

}
