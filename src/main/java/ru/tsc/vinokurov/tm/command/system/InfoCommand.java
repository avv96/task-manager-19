package ru.tsc.vinokurov.tm.command.system;

import ru.tsc.vinokurov.tm.util.NumberUtil;

public final class InfoCommand extends AbstractSystemCommand {

    public static final String NAME = "info";

    public static final String DESCRIPTION = "Show system info.";

    public static final String ARGUMENT = "-i";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final Runtime runtime = Runtime.getRuntime();
        final int processors = runtime.availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = runtime.freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryOutput = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryOutput);
        final long totalMemory = runtime.totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}
