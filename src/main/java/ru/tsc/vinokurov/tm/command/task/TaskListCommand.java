package ru.tsc.vinokurov.tm.command.task;

import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    public static final String NAME = "task-list";

    public static final String DESCRIPTION = "Show task list.";

    public static final String ARGUMENT = null;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.asList(Sort.values()));
        final String sortValue = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortValue);
        final List<Task> tasks = getTaskService().findAll(sort);
        renderTasks(tasks);
    }

}
