package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends  IRepository<Project>{

    Project create(String name);

    Project create(String name, String description);

    Project findOneById(String id);

    boolean existsById(String id);

}
