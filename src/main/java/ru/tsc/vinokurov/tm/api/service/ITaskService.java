package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task>{

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

}
