package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.AbstractModel;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    List<M> findAll();

    List<M> findAll(Comparator comparator);

    M add(M item);

    M remove(M item);

    void clear();

    M findOneByIndex(Integer index);

    M findOneById(String id);

    M removeById(String id);

    M removeByIndex(Integer index);

    int size();

}
