package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task>{

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void clear();


}
