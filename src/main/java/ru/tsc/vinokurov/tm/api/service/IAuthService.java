package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    User register(String login, String password, String email);

    User getUser();

    String getUserId();

    boolean isAuthorized();

}
