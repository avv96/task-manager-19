package ru.tsc.vinokurov.tm.api.repository;

import ru.tsc.vinokurov.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findOneByLogin(String login);

    User findOneByEmail(String email);

    boolean existsByLogin(String login);

    boolean existsByEmail(String email);

}
