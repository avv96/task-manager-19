package ru.tsc.vinokurov.tm.repository;

import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        return items.stream().filter(task -> projectId.equals(task.getProjectId())).collect(Collectors.toList());
    }

}