package ru.tsc.vinokurov.tm.repository;

import ru.tsc.vinokurov.tm.api.repository.IUserRepository;
import ru.tsc.vinokurov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {


    @Override
    public User findOneById(final String id) {
        return items.stream().filter(user -> id.equals(user.getId()))
                .findAny().orElse(null);
    }

    @Override
    public User findOneByLogin(final String login) {
        return items.stream().filter(user -> login.equals(user.getLogin()))
                .findAny().orElse(null);
    }

    @Override
    public User findOneByEmail(final String email) {
        return items.stream().filter(user -> email.equals(user.getEmail()))
                .findAny().orElse(null);
    }

    @Override
    public boolean existsByLogin(final String login) {
        return findOneByLogin(login) != null;
    }

    @Override
    public boolean existsByEmail(final String email) {
        return findOneByEmail(email) != null;
    }

}
