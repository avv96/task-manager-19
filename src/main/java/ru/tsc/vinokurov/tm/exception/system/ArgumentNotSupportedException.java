package ru.tsc.vinokurov.tm.exception.system;

import ru.tsc.vinokurov.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("Error! Argument is not supported...");
    }

    public ArgumentNotSupportedException(String argument) {
        super("Error! Argument ``" + argument + "`` not supported...");
    }
}
